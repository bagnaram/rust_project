fn main() {

    let region1 = "Hey guys";
    let region2= "Невский проспект";
    let southern_germany = "Grüß Gott!";
    let regions = [region1, region2, southern_germany];

    for (i, &region) in regions.iter().enumerate() {
        println!("{}: {}", i + 1, &region);
    }

    let mut a = 5;
    if (a=12) == () {
        println!("testguy {}", a);
    }
    println!("Guess the number!");
    let mut str: String = String::from("testguy testtrr");

    let word = first_word(&str);
    println!("forst word is {}", word);
    str.clear();


}

fn first_word(str: &String) -> &str {
    let chars = str.as_bytes();

    for (i, &char) in chars.iter().enumerate() {
        if char == b' ' {
            println!("Found a space");
            return &str[..i];

        }
    }
    &str
}
